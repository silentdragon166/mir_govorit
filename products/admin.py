from django.contrib import admin

from products.models import Product, ProductWeight


class ProductAdmin(admin.ModelAdmin):
    list_display = fields = ["name", "cooked_count"]


# class ProductWeightAdmin(admin.ModelAdmin):
#     list_display = fields = ["product", "weight"]
#

admin.site.register(Product, ProductAdmin)
# admin.site.register(ProductWeight, ProductWeightAdmin)

from django.db import models


class Product(models.Model):
    name = models.CharField(
        verbose_name="Название продукта", max_length=100, unique=True
    )
    cooked_count = models.PositiveIntegerField(
        verbose_name="Количество приготовленных блюд", default=0
    )

    def __str__(self):
        return self.name


class ProductWeight(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    weight = models.PositiveIntegerField("Вес в граммах")

    def __str__(self):
        return f"{self.product} {self.weight}"

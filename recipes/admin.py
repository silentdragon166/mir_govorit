from django.contrib import admin

from products.models import ProductWeight
from recipes.models import Recipe


class RecipeAdmin(admin.ModelAdmin):
    model = Recipe
    filter_horizontal = ["product_weight"]


admin.site.register(ProductWeight)
admin.site.register(Recipe, RecipeAdmin)

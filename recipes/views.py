from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render

from products.models import Product, ProductWeight
from recipes.models import Recipe


def add_product_to_recipe(request):
    validate, message = validate_add_product_to_recipe_params(request)
    if not validate:
        return HttpResponse(message)

    recipe, product, errors = get_recipe_and_product(request)
    if errors:
        return HttpResponse(errors)

    product_weight, weight = get_product_weight(request, product)
    added_ingredients = recipe.product_weight.all()
    if product_weight in added_ingredients:
        return HttpResponse(
            f"Recipe wasn't changed because of {product} with weight {weight} already in {recipe}"
        )
    elif product.id in added_ingredients.values_list("product__id", flat=True):
        added_product_weight = added_ingredients.get(product__id=product.id)
        recipe.product_weight.remove(added_product_weight)

    recipe.product_weight.add(product_weight)
    return HttpResponse(f"Recipe {recipe} was changed.")


def get_product_weight(request, product):
    weight = int(request.GET.get("weight"))
    product_weight = ProductWeight.objects.filter(product=product, weight=weight)
    if product_weight:
        product_weight = product_weight.last()
    else:
        product_weight = ProductWeight.objects.create(product=product, weight=weight)

    return product_weight, weight


def get_recipe_and_product(request):
    errors = []
    recipe_id = int(request.GET["recipe_id"])
    recipe, error = get_recipe(recipe_id)
    if error:
        errors.append(error)

    product_id = int(request.GET["product_id"])
    product, error = get_product(product_id)
    if error:
        errors.append(error)

    return recipe, product, errors


def get_recipe(recipe_id):
    recipe = Recipe.objects.filter(id=recipe_id)
    if not recipe:
        return None, f"Wasn't found recipe with id {recipe_id}"
    else:
        recipe = recipe.last()

    return recipe, None


def get_product(product_id):
    product = Product.objects.filter(id=product_id)
    if not product:
        return None, f"Wasn't found product with id {product_id}"
    else:
        product = product.last()

    return product, None


def validate_add_product_to_recipe_params(request):
    for parameter in ("recipe_id", "product_id", "weight"):
        value = request.GET.get(parameter)
        if not value:
            return False, f"No required option {parameter}."

        try:
            int(value)
            return True, None
        except ValueError:
            return False, f"Option {parameter} is irrelevant value, api need integer."


def validate_cook_recipe(request):
    value = request.GET.get("recipe_id")
    if not value:
        return False, "No required option recipe_id."

    try:
        int(value)
        return True, None
    except ValueError:
        return False, "Option recipe_id is irrelevant value, api need integer."


def cook_recipe(request):
    validation, message = validate_cook_recipe(request)
    if not validation:
        return HttpResponse(message)

    recipe_id = int(request.GET["recipe_id"])
    recipe, error = get_recipe(recipe_id)
    if error:
        return HttpResponse(error)

    for enum, product_weight in enumerate(recipe.product_weight.all()):
        product = product_weight.product
        product.cooked_count += 1
        product.save()

    return HttpResponse(f"{enum + 1} products was changed.")


def validate_show_recipes_without_product(request):
    value = request.GET.get("product_id")
    if not value:
        return False, "No required option product_id."

    try:
        int(value)
        return True, None
    except ValueError:
        return False, "Option product_id is irrelevant value, api need integer."


def show_recipes_without_product(request):
    validation, message = validate_show_recipes_without_product(request)
    if not validation:
        return HttpResponse(message)

    product_id = int(request.GET["product_id"])
    product, error = get_product(product_id)
    if error:
        return HttpResponse(error)

    recipies = Recipe.objects.filter(
        ~Q(product_weight__product=product) | Q(product_weight__weight__lt=10)
    ).distinct()
    if not recipies:
        return HttpResponse(f"Product {product} are in all recipies.")

    return render(request, "recipies/recipies_list.html", {"recipies": recipies})

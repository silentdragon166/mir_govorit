from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import m2m_changed
from django.dispatch import receiver

from products.models import ProductWeight


class Recipe(models.Model):
    name = models.CharField("Название", max_length=250, unique=True)
    product_weight = models.ManyToManyField(
        to=ProductWeight, related_name="recipe", null=True, blank=True
    )

    def __str__(self):
        return self.name


@receiver(m2m_changed, sender=Recipe.product_weight.through)
def check_product_duplicates(sender, **kwargs):
    instance = kwargs.pop("instance", None)
    all_products_ids = instance.product_weight.all().values_list("product", flat=True)
    if len(all_products_ids) != len(set(all_products_ids)):
        raise ValidationError(
            "Один и тот же продукт не может быть использован дважды в одном рецепте"
        )
